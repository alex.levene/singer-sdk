# [Singer SDK Implementation Details](/.README.md) - Tap Metrics

_**Note:**: The SDK does not yet generate metrics as a part of tap output. This
work is [tracked here](https://gitlab.com/meltano/singer-sdk/-/issues/91) for future
development._

## See Also

- [Singer Spec: Metrics](https://meltano.com/docs/singer-spec.html#metrics)
